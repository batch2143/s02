CREATE DATABASE blog_db;

CREATE TABLE posts (
	id INT NOT NULL,
	author INT NOT NULL,
	title VARCHAR (500) NOT NULL,
	content VARCHAR (500) NOT NULL,
	datetime_posted DATETIME NOT NULL,
	user_id INT NOT NULL,
	post_likes_id INT NOT NULL,
	post_comments_id INT NOT NULL,
	PRIMARY KEY (id),
/*	CONSTRAINT fk_posts_post_likes_id
		FOREIGN KEY (post_likes_id) REFERENCES post_likes(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_posts_post_comments_id
		FOREIGN KEY (post_comments_id) REFERENCES post_comments(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT*/
);		

CREATE TABLE users (
	id INT NOT NULL,
	email VARCHAR (100) NOT NULL,
	password VARCHAR (300) NOT NULL,
	datetime_created DATETIME NOT NULL,
	posts_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_users_posts_id
		FOREIGN KEY (posts_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);		

CREATE TABLE post_comments (
	id INT NOT NULL,
	content VARCHAR (5000) NOT NULL,
	datetime_commented DATETIME NOT NULL,
	posts_id INT NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_comments_posts_id
		FOREIGN KEY (posts_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_comments_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT		
);

CREATE TABLE post_likes (
	id INT NOT NULL,
	datetime_liked DATETIME NOT NULL,
	posts_id INT NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_likes_posts_id
		FOREIGN KEY (posts_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_likes_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT		
);		
