-- To run MySQL/Maria DB

	-- mysql -u root
	-- -u stands for username
	-- - root is the default
	-- - stands for password

-- showing/retrieving all database
SHOW DATABASES;

-- commands in sql will still work with all lowercase letters
-- using al Caps allow for code readability to distinguish commands with tabe names, column names and vaue inputs
-- make sure semi-colon are added at the end of the syntax

--create a database 
--syntax: CREATE DATABASE database_name;

CREATE DATABASE music_store;

-- dropping/deleting a database
-- syntax: DROP DATABASE database_name;

DROP DATABASE music_store;

-- recreate our music database

CREATE DATABASE music_db;

-- select a database
-- syntax; USE database_name;

USE music_db;

-- creating/adding tables:
-- syntax
/*	CREATE TABLE table_name(
			column1,
			column2,
			PRIMARY KEY (id)
		);*/

CREATE TABLE singers (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);		

-- showing/retrieving tables;
SHOW TABLES;

-- drop/deleting tables

DROP TABLE singers;

-- create 'artists' table

CREATE TABLE artists (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);		

-- Describing tables allows to see the table columns, data types and extra option set
DESCRIBE artists;

-- Create tables with foreign key
-- syntax:
	
	CONSTRAINT foreign_key_name
		FOREIGN KEY (column name)
		REFERENCES table_name(id) REFERENCES table_name (id)
		ON UPDATE ACTION (CASCADE), NO ACTION, SET NULL, SET DEFAULT
		ON DELETE ACTION (RESTRICT, SET NULL)

CREATE TABLE records (
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(25) NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists (id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);		


-- RENAME 'records' table to 'albums'
-- SYNTAX:

ALTER TABLE table_name
	RENAME to new_table_name;

ALTER TABLE records
	RENAME TO albums;

-- Creating/ adding columns to a table
-- syntax:
	ALTER TABLE new_table_name
		ADD COLUMN_name data_type extra_options;

ALTER TABLE albums
	ADD date_released DATE NOT NULL;

-- dropping/deleting columns:
-- syntax
	ALTER TABLE new_table_name
		DROP COLUMN column_name;

ALTER TABLE albums
	DROP COLUMN date_released;

-- addind a column to a specific postion
-- syntax
	ALTER TABLE table_name
		ADD column_name data_type extra_options
			AFTER column_name

ALTER TABLE albums
	ADD year DATE NOT NULL
		AFTER album_title;

-- modifying a column
-- syntax

	ALTER TABLE table_name
		MODIFY column_name data_type extra_options

ALTER TABLE albums
	MODIFY album_title VARCHAR(50) NOT NULL;

-- renaming a column 
-- syntax:
	ALTER TABLE table_name
		CHANGE COLUMN old_name new_name data_type extra_options;

ALTER TABLE albums
	CHANGE COLUMN year date_released DATE NOT NULL;

-- specifying the data_type and extra_options is required even if the column definition do not change


-- create users table
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR (50) NOT NULL,
	password VARCHAR (50) NOT NULL,
	full_name VARCHAR (50) NOT NULL,
	contact_number INT (50) NOT NULL,
	email VARCHAR (50),
	adress VARCHAR (50),
	PRIMARY KEY (id)
)

-- create songs table
CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR (50) NOT NULL,
	length TIME NOT NULL,
	genra VARCHAR (50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE RESTRICT
);

CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE	
		ON DELETE RESTRICT
);




-- Create playlists_songs table
CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);
